package main

import (
	"bufio"
	"context"
	"log"
	"os"
	"strconv"

	"googlemaps.github.io/maps"
)

func main() {
	c, err := maps.NewClient(maps.WithAPIKey("<your_api_key_here"))
	if err != nil {
		log.Fatalf("fatal error: %s", err)
	}
	r := &maps.DistanceMatrixRequest{
		Origins:      []string{"<your_origin", "locations"},
		Destinations: []string{"<your_destination>"},
		Mode:         "ModeDriving",
	}
	distance, err := c.DistanceMatrix(context.Background(), r)
	if err != nil {
		log.Fatalf("fatal error: %s", err)
	}
	f, _ := os.Create("/tmp/commutes/data.prom")
	if err != nil {
		log.Fatalf("Unable to open stats file: %s", err)
	}
	defer f.Close()
	w := bufio.NewWriter(f)
	for i, v := range distance.Rows {
		//t := time.Unix
		w.WriteString("drive_time{label_name =\"" + r.Origins[i] + "\"} " + strconv.FormatInt(v.Elements[0].Duration.Nanoseconds(), 10) + "\n")
		w.Flush()
	}
}
