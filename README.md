# What is this?
This uses go-lang to touch the Google maps Distance API and read times in nanoseconds and output to a file that is *mostly* prometheus compliant. To read that with prometheus use prometheus_node_explorer with the text-collector turned on and pointed at the directory not the file.
